require('popper.js');
require('bootstrap');
const $ = require('jquery');
require('jquery-mask-plugin');

$(document).ready(function () {
    var bouqetModal = $('#bouquetModal'),
        orderModal = $('#orderModal'),
        responseModal = $('#responseModal'),
        pickedBouquet = null;

    $('.js-navbarToggler').click(function () {
        $('.js-navbarDropdown').addClass('-open');
        $('body').addClass('-overflow');
    });

    $('.js-navbarClose').click(function () {
        $('.js-navbarDropdown').removeClass('-open');
        $('body').removeClass('-overflow');
    });

    $('.js-phoneInput').mask("+7(999) 999-99-99");
    $('.js-phoneInputGoods').mask("+7(999) 999-99-99");

    $('.js-requestCall').click(function (e) {
        e.preventDefault();

        let input = $('.js-phoneInput');

        if (input.val() === '' || input.val().length !== 17) {
            input.focus();
        } else {

             $.post({
               url: "./mail.php",
               data : {
                   type : 'requestCall',
                   phone : input.val()
               },
               success: function (data){
                   let obj = JSON.parse(data);

                   if (obj.status === 200) {
                       input.val('');
                        responseModal.modal();

                        setTimeout(function () {
                            responseModal.modal('hide');
                        }, 5000);
                   }
               }
           });
        }

       
    });

    $('.js-requestGoods').click(function () {
        let name = $('.js-nameInputGoods');
        let phone = $('.js-phoneInputGoods');

        if (name.val() === '') {
            name.focus();
        } else if (phone.val() === '' || phone.val().length !== 17) {
            phone.focus();
        } else {
            var data = {
                type : 'requestGoods',
                name : name.val(),
                phone : phone.val()
            };

            data.bouquet = pickedBouquet !== null ? pickedBouquet : 'Букет не выбран';

            $.post({
                url: "./mail.php",
                data : data,
                success: function (data){
                    let obj = JSON.parse(data);

                    if (obj.status === 200) {
                        onSuccessForm();
                    }
                }
            });
        }
    });

    function onSuccessForm() {
        $('.js-nameInputGoods').val('');
        $('.js-phoneInputGoods').val('');

        orderModal.modal('hide');

        setTimeout(function () {
            responseModal.modal();
        }, 200);

        setTimeout(function () {
            responseModal.modal('hide');
        }, 5000);
    }

    $('.js-openOrderModal').click(function () {
        pickedBouquet = bouqetModal.find('.name').text();
        bouqetModal.modal('hide');

        setTimeout(function () {
            orderModal.modal();
        }, 200);
    });

    $('.js-orderModalFromCatalog').click(function () {
        pickedBouquet = $(this).parent().find('.name').text();
    });

    $('.js-heroBtn').click(function () {
        orderModal.modal();
    });

    // Bouqet Header

    function getHeader(item) {
        return $(item).find('.name').text();
    }

    function getPriceButtonText(item) {
        return $(item).find('.btn').html();
    }

    function getDescriptionText(item) {
        return $(item).find('.description').text();
    }

    function getBgColor(item) {
        return $(item).find('.content').css('background-color');
    }

    function slideTemplate(imageSource) {
        return '' + '<div class="carousel-item">' + '<div class="carousel-item-inner">' + '<img class="d-block img-fluid" src="' + imageSource + '" alt="">' + '</div>' + '</div>';
    }

    function generateSlider(item) {

        return $(item).find('.additional-images img').toArray().map(function (image) {
            return slideTemplate($(image).attr('src'));
        }).reduce(function (a, b) {
            return a + b;
        });
    }

    function activateSlider() {
        bouqetModal.find('.carousel-inner .carousel-item')[0].classList.add('active');
    }

    $("*[data-custom-target='#bouquetModal']").click(function (e) {
        if (!e.target.hasAttribute('data-toggle') && !e.target.hasAttribute('href')) {
            bouqetModal.find('.name').text(getHeader(this));
            bouqetModal.find('.btn').html(getPriceButtonText(this));
            bouqetModal.find('.description').text(getDescriptionText(this));
            bouqetModal.find('.carousel-inner').html(generateSlider(this)).css('background-color', getBgColor(this));
            activateSlider();
            bouqetModal.modal('show');
        }
    });

    $('a').click(function () {
        if ($(this).attr('href') == "#carouselImg") {
            
        } else if ($(this).attr('href').match(/#/) == "#") {
            event.preventDefault();
            $('.js-navbarDropdown').removeClass('-open');
            var id = $(this).attr('href');
            var top = $(id).offset().top;

            $('body, html').animate({scrollTop: top}, 1000);
        }
    });
});